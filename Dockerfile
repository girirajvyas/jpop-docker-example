# pull JDK 8 from DockerHub
FROM openjdk:8

# Copy current code in Docker directory
COPY . /usr/jpop-docker-example 

# Select the working directory
WORKDIR /usr/jpop-docker-example/src

# Compile the main class
RUN javac com/epam/jpop/helloworld/Main.java 

# Execute command
CMD ["java","com.epam.jpop.helloworld.Main"]